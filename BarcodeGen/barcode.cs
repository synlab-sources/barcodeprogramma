﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarcodeGen
{
    public class Barcode
    {
        public int _Id;
        public string _barcode;
        public static List<Barcode> barcodes = new List<Barcode>();

        public Barcode(int id, string barcode)
        {
            this._Id = id;
            this._barcode = barcode;
            barcodes.Add(this);
        }
        public static List<Barcode> GetBarcodes()
        {
            return barcodes;
        }
        public sealed class BarcodesMap : ClassMap<Barcode>
        {
            public BarcodesMap()
            {
                Map(m => m._Id).Index(0).Name("id");
                Map(m => m._barcode).Index(1).Name("barcode");
            }
        }
    }
}
