﻿using CsvHelper;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace BarcodeGen
{

    public partial class Form1 : Form
    {
        const string RegExMatch = "[A][H][A-Z]{2}[0-9]{3}|[D][E][S][T][0-9]{3}|[E][C][C][A][0-9]{3}|[P][D][L][C][0-9]{3}";
        public static SqlConnection myConn = new SqlConnection("Data Source = NL3017131\\SQLEXPRESS; Initial Catalog = master; Integrated Security = True; Connect Timeout = 30");
        public string FileLocation;
        public int maxVal;
        public string barcode;
        
        public Form1()
        {
            InitializeComponent();
            txtFirstBarcode.Text = Properties.Settings.Default.Startbarcode;
            txtNumber.Text = Properties.Settings.Default.Aantal.ToString();

        }

        private void btnCreateDatabase_Click(object sender, EventArgs e)
        {

            CreateDatabase();
            CreateTable();
            FillTable();

        }



        private static void CreateDatabase()
        {
            String str;
            String commandType;
            commandType = "DropDatabase";
            str = " alter database BarcodeDatabase set single_user with rollback immediate; DROP Database BarcodeDatabase";
            try
            { ExecuteDbCommand(str, commandType); }
            finally
            {
                commandType = "CreateDatabase";
                str = "CREATE DATABASE BarcodeDatabase ON PRIMARY " +
                    "(NAME = BarcodeDatabase_Data, " +
                    "FILENAME = 'C:\\db\\BarcodeDatabaseData.mdf', " +
                    "SIZE = 2MB, MAXSIZE = 100MB, FILEGROWTH = 10%) " +
                    "LOG ON (NAME = BarcodeDatabase_Log, " +
                    "FILENAME = 'C:\\db\\BarcodeDatabaseLog.ldf', " +
                    "SIZE = 1MB, " +
                    "MAXSIZE = 50MB, " +
                    "FILEGROWTH = 10%)";

                ExecuteDbCommand(str, commandType);
            }
        }

        private void CreateTable()
        {
            String str;
            String commandType;
            commandType = "CreateTable";
            {

                str = " use BarcodeDatabase; CREATE TABLE Barcodes" +
                 "(Id INTEGER CONSTRAINT PKeyMyId PRIMARY KEY," +
                    "Name CHAR(50))";

                ExecuteDbCommand(str, commandType);
            }
        }


        private void FillTable()
        {
            String str;
            String commandType;

            int val;
            string barcode;
            barcode = txtFirstBarcode.Text;
            if (Int32.TryParse(txtNumber.Text, out val))
            {
                for (int teller = 1; teller <= val; teller++)
                {
                    var rMatch = Regex.Match(barcode, RegExMatch);
                    if (!rMatch.Success)
                    {
                        str = $" use BarcodeDatabase; insert into [Barcodes](Id, Name) values ({teller} ,'{barcode}') ";
                        commandType = "FillTable";
                        ExecuteDbCommand(str, commandType);
                    }
                    else
                    {
                        teller -= 1;
                    }

                    barcode = generateBarcode(barcode);
                    lblBarcode.Text = barcode;
                }
            }
            else
            {
                MessageBox.Show("not a valid number in the barcode field");
            }


            Properties.Settings.Default.Startbarcode = generateBarcode(barcode);
            Properties.Settings.Default.Aantal = Int32.Parse(txtNumber.Text);
        }

        private string generateBarcode(string barcode)
        {

            char[] newbarcode = new char[7];//0-6
            char[] barcodeAsChars = barcode.ToCharArray();
            string temp = "";
            char tempChar;
            int tempInt = 0;
            int i = 6;
            bool Ready = false;
            while (Ready == false)
            {
                Debug.WriteLine(barcodeAsChars[i]);
                temp += barcodeAsChars[i];
                tempChar = barcodeAsChars[i];
                if (char.IsDigit(tempChar))
                {
                    tempInt = (int)Char.GetNumericValue(barcodeAsChars[i]);
                    if (tempInt < 9)
                    {
                        //temp = (int.Parse(temp) + 1).ToString();
                        tempInt += 1;// (int.Parse(tempChar) + 1).ToString();
                        for (int j = 0; j != i; j++)
                            newbarcode[j] = barcodeAsChars[j];
                        newbarcode[i] = tempInt.ToString()[0];

                        Ready = true;
                    }
                    else
                    //laatste cijfer wordt 0; voorlaatste 1 omhoog.
                    {
                        tempInt = 0;
                        newbarcode[i] = tempInt.ToString()[0];

                    }
                }
                else
                {
                    tempChar = Char.ToUpper(tempChar);

                    //A-Z
                    if (tempChar < 'Z')
                    {
                        int letter = tempChar + 1;
                        tempChar = (Char)letter;

                        for (int j = 0; j != i; j++)
                            newbarcode[j] = barcodeAsChars[j];
                        newbarcode[i] = tempChar;
                        Ready = true;
                    }
                    else
                    {
                        tempChar = 'A';
                        newbarcode[i] = tempChar;
                    }
                }
                i -= 1;
            }
            return string.Join("", newbarcode);

        }




        private static void ExecuteDbCommand(string str, string commandType)
        {
            SqlCommand myCommand = new SqlCommand(str, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                // MessageBox.Show($"{commandType} is executed Successfully {Environment.NewLine} {str}", "MyProgram", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch (System.Exception ex)
            {
                if (!ex.Message.Contains("because it does not exist"))
                {
                    MessageBox.Show(ex.ToString(), commandType, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }

        private void btnCSV_Click(object sender, EventArgs e)
        {
            var saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "CSV files|*.csv";
            saveFileDialog1.ShowDialog();

            FileLocation = saveFileDialog1.FileName;
            if (FileLocation != "")
            {



                barcode = txtFirstBarcode.Text;
                if (Int32.TryParse(txtNumber.Text, out maxVal))
                {
                    progressBar1.Maximum = 100;
                    backgroundWorker.RunWorkerAsync();


                }
                else
                {
                    MessageBox.Show("not a valid number in the barcode field");
                }



            }
        }
        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
             backgroundWorker = sender as BackgroundWorker;
            for (int teller = 1; teller <= maxVal; teller++)
            {
                var rMatch = Regex.Match(barcode, RegExMatch);
                if (!rMatch.Success)
                {
                    var BarcodeRecord = new Barcode(teller, barcode);
                }
                else
                {
                    teller -= 1;
                }
                barcode = generateBarcode(barcode);
                // lblBarcode.Text = barcode;
              var   percentage = (double)((double)teller /(double)maxVal * 100);
                this.backgroundWorker.ReportProgress( (int)percentage);
               
            }
            var records = Barcode.GetBarcodes();
            using (var writer = new System.IO.StreamWriter(FileLocation))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.Configuration.RegisterClassMap<Barcode.BarcodesMap>();

                csv.WriteRecords(records);
            }
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.progressBar1.Value = e.ProgressPercentage;
            lblBarcode.Text = barcode;
        }
        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (e.Error != null)
            {
                resultLabel.Text = "Error: " + e.Error.Message;
            }
            else
            {
                Properties.Settings.Default.Startbarcode = lblBarcode.Text;
                Properties.Settings.Default.Aantal = Int32.Parse(txtNumber.Text);
                Properties.Settings.Default.Save();
            }
        }



    }
}

